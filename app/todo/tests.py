from django.test import TestCase

# Create your tests here.
from .models import ToDo


class ToDoModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        ToDo.objects.create(title='Here is todo title.')
        ToDo.objects.create(description='Here is todo description.')
        ToDo.objects.create(completed=False)

    def test_todo_list_title(self):
        todo = ToDo.objects.get(id=1)
        expected_title = f'{todo.title}'
        self.assertEquals(expected_title, 'Here is todo title.')

    def test_todo_list_description(self):
        todo = ToDo.objects.get(id=2)
        expected_description = f'{todo.description}'
        self.assertEquals(expected_description, 'Here is todo description.')

    def test_todo_list_completed(self):
        todo = ToDo.objects.get(id=3)
        expected_completed = todo.completed
        self.assertEquals(expected_completed, False)
