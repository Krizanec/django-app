from django.urls import path
from . import views

urlpatterns = [
    path('', views.ToDoList.as_view()),
    path('hello/', views.index),
    path('<int:pk>/', views.DetailTodo.as_view()),
]