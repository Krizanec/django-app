from rest_framework import generics
from rest_framework.response import Response
from django.shortcuts import render, HttpResponse, redirect

from .models import ToDo
from .serializers import ToDoSerializer
from .forms import ToDoForm


# Create your views here.
class ToDoList(generics.ListCreateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer


class DetailTodo(generics.RetrieveUpdateDestroyAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer


def index(request):
    # return HttpResponse("hello")
    # return render(request, 'index.html')
    form = ToDoForm()
    tasks = ToDo.objects.all()

    if request.method == "POST":
        # Get the posted form
        form = ToDoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("")
    return render(request, "index.html", {"task_form": form,  "tasks": tasks})

